"""Use built-in abc to implement Abstract
classes and methods"""
from abc import ABC, abstractmethod
  
"""Class Dedicated to Command"""
class Command(ABC):
      
    """constructor method"""
    def __init__(self, receiver):
        self.receiver = receiver
      
    """process method"""
    def excecute(self):
        pass
    
    def unexecute(self):
        pass
  
"""Class dedicated to Command Implementation"""
class TurnOnCommandImplementation(Command):
      
    """constructor method"""
    def __init__(self, receiver):
        self.receiver = receiver
  
    """process method"""
    def execute(self):
        self.receiver.turn_on()
        
    def unexecute(self):
        self.receiver.turn_off()

"""Class dedicated to Command Implementation"""
class TurnOffCommandImplementation(Command):
      
    """constructor method"""
    def __init__(self, receiver):
        self.receiver = receiver
  
    """process method"""
    def execute(self):
        self.receiver.turn_off()
        
    def unexecute(self):
        self.receiver.turn_on()
  
"""Class dedicated to Receiver"""
class MaseratiReceiver:
      
    """perform-action method"""
    def turn_on(self):
        print('Maserati is turned on')
    
    def turn_off(self):
        print("Maserati is turned off.")
        
"""Class dedicated to Receiver"""
class LampReceiver:
      
    """perform-action method"""
    def turn_on(self):
        print('Lamp is turned on')
    
    def turn_off(self):
        print("Lamp is turned off.")
        
"""Class dedicated to Receiver"""
class RadioReceiver:
      
    """perform-action method"""
    def turn_on(self):
        print('Radio is turned on')
    
    def turn_off(self):
        print("Radio is turned off.")
        
"""Class dedicated to Receiver"""
class TVReceiver:
      
    """perform-action method"""
    def turn_on(self):
        print('TV is turned on')
    
    def turn_off(self):
        print("TV is turned off.")
  
"""Class dedicated to Invoker"""
class Invoker:
      
    """command method"""
    def set_command(self, cmd):
        self.cmd = cmd
  
    """execute method"""
    def execute(self):
        self.cmd.execute()
     
    """unexecute method"""   
    def unexecute(self):
        self.cmd.unexecute()
        
class AdvancedInvoker:
    def __init__(self):
        self.cmd_list = []
        self.executed_cmds = []    
        
    """command method"""
    def add_commands(self, new_cmd_list):
        for item in new_cmd_list:
            self.cmd_list.append(item)
          
    """execute method"""
    def execute(self):
        next_command = self.cmd_list.pop()
        next_command.execute()
        self.executed_cmds.append(next_command)
        
    """unexecute method"""    
    def unexecute(self):
        if len(self.executed_cmds)>0:
            last_command = self.executed_cmds.pop()
            last_command.unexecute()
            
  
"""main method"""
if __name__ == "__main__":
      
    #create Receiver objects
    mylamp = LampReceiver()
    mymaserati = MaseratiReceiver()
    myradio = RadioReceiver()
    myTV = TVReceiver()
    
    #create command implementations.
    turn_on_lamp_cmd = TurnOnCommandImplementation(mylamp)
    turn_off_lamp_cmd = TurnOffCommandImplementation(mylamp)
    turn_on_maserati_cmd = TurnOnCommandImplementation(mymaserati)
    turn_off_maserati_cmd = TurnOffCommandImplementation(mymaserati)
    turn_on_radio_cmd = TurnOnCommandImplementation(myradio)
    turn_off_radio_cmd = TurnOffCommandImplementation(myradio)
    turn_on_TV_cmd = TurnOnCommandImplementation(myTV)
    turn_off_TV_cmd = TurnOffCommandImplementation(myTV)    
    
    # create Invoker
    myremote = Invoker()
    
    
    # Do something:
    print("\n>>>Play with remote:\n")
    myremote.set_command(turn_off_maserati_cmd)   
    myremote.execute()    
    
    myremote.set_command(turn_on_lamp_cmd)   
    myremote.execute()
    
    myremote.set_command(turn_on_radio_cmd)   
    myremote.execute()  
    myremote.set_command(turn_off_radio_cmd)   
    myremote.execute()  
      
    myremote.set_command(turn_on_TV_cmd)   
    myremote.execute()    
    myremote.unexecute()
   

    #create a invoker that keeps track of cmds:
    my_smart_remote = AdvancedInvoker()
    
    turn_all_on_cmd_list = [turn_on_lamp_cmd, turn_on_radio_cmd, turn_on_maserati_cmd, turn_on_TV_cmd]
    my_smart_remote.add_commands(turn_all_on_cmd_list)
    #turn all on
    print("\n>>>Turning all devices on:\n")
    my_smart_remote.execute() 
    my_smart_remote.execute() 
    my_smart_remote.execute() 
    my_smart_remote.execute() 
    print("\n>>>Undo the past two commands:\n")
    # undo last two
    my_smart_remote.unexecute() 
    my_smart_remote.unexecute() 

    
